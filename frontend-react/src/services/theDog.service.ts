import { instanceTheDog } from "@/configuration/config-TheDog"

export const getCharacters = async () => {
    //https://api.thedogapi.com/v1/images/search?offset=12&limit=3
    const response = await instanceTheDog.get('/character')    
    return response.data
}
