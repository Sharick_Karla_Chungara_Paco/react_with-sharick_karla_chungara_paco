import axios from "axios";

export const instanceTheDog = axios.create({
    baseURL: process.env.NEXT_PUBLIC_API_THE_DOG,
    timeout: 5000,
    headers: { 'X-Custom-Header': 'foobar' }
});

instanceTheDog.interceptors.request.use(function (config) {    
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});