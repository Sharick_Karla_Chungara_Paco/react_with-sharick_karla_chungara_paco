import { getCharacters } from "@/services/theDog.service"
import { useEffect, useState } from "react"

export const useTheDog = () => {
    const [characters,setCharacters] = useState<any>()
    const [loading,setLoading] = useState<boolean>(true)

    const fetchData = async () => {
        try{
            const responseCharacters = await getCharacters()
            setCharacters(responseCharacters)
            setLoading(false)
        }catch(e){
            console.log(e)
            setLoading(false)
        }
        
    }
    useEffect(() => {
        fetchData()
    },[])

    return {characters,loading}
}