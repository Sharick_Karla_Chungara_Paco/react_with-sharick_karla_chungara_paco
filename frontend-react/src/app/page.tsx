"use client"

import Image from "next/image";
import styles from "./page.module.css";
import Navbar from "./Components/Navbar";
import CajitaHeader from "./Components/CajitaHeader";
import Botton from "./Components/Botton";
import Title from "./Components/Title";
import * as React from 'react';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import BoxInfo from "./Components/BoxInfo";
import LocationOnIcon from '@mui/icons-material/LocationOn';
import Rating from '@mui/material/Rating';
import Typography from "@mui/material/Typography";
import Redes from "./Components/Redes";
import Photo from "./Components/Photo";
import { useTheDog} from '@/hooks/useTheDog';



  export default function Home() {
    const { characters, loading} = useTheDog()
    console.log(loading?'CARGANDO...': '')
   if(!loading){
    console.log(characters)
   }
  return (
    <main>
      <div className="Header">
        <Navbar></Navbar>
            <Image src="/images/Fondo-header" alt="" width={100} height={100} />
        <div className="header-text">
            <h1>We Find The Best Tours For You</h1>
            <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. <br/>Velit officia consequat duis enim velit mollit. Exercitation veniam consequat <br/> sunt nostrud amet.</p>
            <div className="header-iconoVideo">
            <Image src="/images/iconoVideo.svg" alt="" width={100} height={100} />
            <h3>Watch Video</h3>
            </div>
            
        </div>
        <div className="Caja-header">
          <CajitaHeader imageUrl="/images/mapa.svg" title="Location" text="Search For A Destination"></CajitaHeader>
          <hr />
          <CajitaHeader imageUrl="/images/Personitas.svg" title="Guests" text="How many Guests?"></CajitaHeader>
          <hr />
          <CajitaHeader imageUrl="/images/calendary.svg" title="Date" text="Pick a date"></CajitaHeader>
          <div className="Boton2">
              <Botton boton="Saerch" border></Botton>
          </div>
          
        </div>
    
      </div>
      <div className="container1">
        <div className="elipse">
          <Image src="/images/elipse.svg" alt=""width={565.24} height={581.26} />
        </div>
        <div className="contenido">
          <button>Trending Now</button>
          <h2>Wilderlife of Alaska</h2>
          <div className="texto">
          <h6>{<LocationOnIcon />}Alaska, USA</h6>
          <hr />
          
          <Stack spacing={1}>
            <Rating name="half-rating" defaultValue={2.5} precision={0.5} />
           
          </Stack>
          <div>
          <h6>4.9</h6>
          <h6>(300 reviews)</h6>

          </div>
          

        
      
          
          </div>
          <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
          
      
          

        </div>
      </div>

      <div className="container2">
        <div className="part1">
          <Title title="From The Gallery" parrafo="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit"></Title>
          <button className="btn-secundary">View All Images</button>
        </div>
        <div className="part2">
          <div className="images1">
          <Photo imgUrl="/images/perro1.jpg"></Photo>
          <Photo imgUrl="/images/perro2.jpg"></Photo>
          <Photo imgUrl="/images/perro3.jpg"></Photo>
          <Photo imgUrl="/images/perro4.jpg"></Photo>
          </div>
          <div className="images2">
          <Photo imgUrl="/images/perro5.jpg"></Photo>
          <Photo imgUrl="/images/perro6.jpg"></Photo>
          <Photo imgUrl="/images/perro7.jpg"></Photo>
          <Photo imgUrl="/images/perro8.jpg"></Photo>
          </div>
        </div>
      </div>
      <div className="container3">
        <div className="part1">
          <Title title="Latest Stories" parrafo="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit"></Title>
          <button className="btn-secundary">View All Posts</button>
        </div>
        
        <Grid container spacing={2} sx={{marginY:"40px"}}>
              <Grid item xs={12}>
              <Box sx={{height:"70px",display: "flex",alignItems:"center", justifyContent: "space-between", marginBottom: "20px"}}>
                 
                
                </Box>
              </Grid>
              {characters?.results.map((item: any, index: number) => {
                
                return (
                  <Grid key={index} item xs={12} sm={12} md={6} lg={3} xl={3}>
                    <BoxInfo
                      avatar={item.name.split('')[0]}
                      name={item.name}
                      image={item.image}
                      date={item.location.name}
                      title={item.title}  >
                    </BoxInfo>
                  </Grid>
                )
              })}
            </Grid>

        <Box sx={{display:"flex", justifyContent:"space-between", width:"100%", height:"350px", backgroundColor:"#13253F",marginRight:"130px"}}>
          <Box sx ={{marginLeft:"130px",marginTop:"70px"}}>
            <Typography sx ={{color:"rgba(255, 255, 255, 0.8)",fontFamily: "Mulish",fontOpticalSizing: "auto",fontStyle: "normal",fontWeight: "bold"}} variant="caption" display="block" gutterBottom>
            Language
            </Typography>
            <img src="/images/1.svg" alt="" />
            
            <Typography sx ={{color:"#FFFFFF",fontFamily: "Mulish",fontOpticalSizing: "auto",fontStyle: "normal",fontWeight: "bold"}}variant="caption" display="block" gutterBottom>
            Currency
            </Typography>
            <img src="/images/2.svg" alt="" />
            
          </Box>
          <Box sx ={{marginTop:"70px"}}>
            <Typography  sx ={{color:"#FFFFFF",fontFamily: "Mulish",fontOpticalSizing: "auto",fontStyle: "normal",fontWeight: "bold",lineHeight:"25px"}}variant="caption" display="block" gutterBottom>
            Company </Typography>
            <Typography sx ={{color:"rgba(255, 255, 255, 0.6)",fontFamily: "Mulish",fontOpticalSizing: "regular",fontStyle: "normal",lineHeight:"25px"}} variant="caption" display="block" gutterBottom>
            About Us </Typography>
            <Typography sx ={{color:"rgba(255, 255, 255, 0.6)",fontFamily: "Mulish",fontOpticalSizing: "regular",fontStyle: "normal",lineHeight:"25px"}} variant="caption" display="block" gutterBottom>
            Blog </Typography>
            <Typography sx ={{color:"rgba(255, 255, 255, 0.6)",fontFamily: "Mulish",fontOpticalSizing: "regular",fontStyle: "normal",lineHeight:"25px"}} variant="caption" display="block" gutterBottom>
            Press Room </Typography>
            <Typography sx ={{color:"rgba(255, 255, 255, 0.6)",fontFamily: "Mulish",fontOpticalSizing: "regular",fontStyle: "normal",lineHeight:"25px"}}variant="caption" display="block" gutterBottom>
            Language </Typography>
          </Box>
          <Box sx ={{marginTop:"70px"}}>
            <Typography sx ={{color:"rgba(255, 255, 255, 0.8)",fontFamily: "Mulish",fontOpticalSizing: "auto",fontStyle: "normal",fontWeight: "bold"}} variant="caption" display="block" gutterBottom>
            Help </Typography>
            <Typography sx ={{color:"rgba(255, 255, 255, 0.6)",fontFamily: "Mulish",fontOpticalSizing: "regular",fontStyle: "normal",lineHeight:"25px"}} variant="caption" display="block" gutterBottom>
            Contact us </Typography>
            <Typography sx ={{color:"rgba(255, 255, 255, 0.6)",fontFamily: "Mulish",fontOpticalSizing: "regular",fontStyle: "normal",lineHeight:"25px"}} variant="caption" display="block" gutterBottom>
            FAQs </Typography>
            <Typography sx ={{color:"rgba(255, 255, 255, 0.6)",fontFamily: "Mulish",fontOpticalSizing: "regular",fontStyle: "normal",lineHeight:"25px"}} variant="caption" display="block" gutterBottom>
            Terms and conditions </Typography>
            <Typography sx ={{color:"rgba(255, 255, 255, 0.6)",fontFamily: "Mulish",fontOpticalSizing: "regular",fontStyle: "normal",lineHeight:"25px"}} variant="caption" display="block" gutterBottom>
            Privacy policy</Typography>
            <Typography sx ={{color:"rgba(255, 255, 255, 0.6)",fontFamily: "Mulish",fontOpticalSizing: "regular",fontStyle: "normal",lineHeight:"25px"}} variant="caption" display="block" gutterBottom>
            Sitemap</Typography>
           
          </Box>
          <Box sx ={{marginTop:"70px", marginRight:"130px",lineHeight:"25px"}}>
          <Typography sx ={{color:"rgba(255, 255, 255, 0.8)",fontFamily: "Mulish",fontOpticalSizing: "auto",fontStyle: "normal",fontWeight: "bold",lineHeight:"25px"}}variant="caption" display="block" gutterBottom>
          Payment methods possible</Typography>
          <div className="empresas" >
          <div className="empresas1" >
            <img src="/images/empre1.svg" alt="" />
            <img src="/images/empre2.svg" alt="" />
            <img src="/images/empre3.svg" alt="" />
            <img src="/images/empre4.svg" alt="" />
            <img src="/images/empre5.svg" alt="" />
          </div>
          <div className="empresas2">
            <img src="/images/empre6.svg" alt="" />
            <img src="/images/empre7.svg" alt="" />
            <img src="/images/empre8.svg" alt="" />
            <img src="/images/empre9.svg" alt="" />
            <img src="/images/empre10.svg" alt="" />
          </div>


          </div>
          
          <Typography  sx ={{color:"#FFFFFF",fontFamily: "Mulish",fontOpticalSizing: "auto",fontStyle: "normal",fontWeight: "bold",lineHeight:"50px"}}variant="caption" display="block" gutterBottom>
          Company </Typography>
          <Typography sx ={{color:"rgba(255, 255, 255, 0.6)",fontFamily: "Mulish",fontOpticalSizing: "regular",fontStyle: "normal",lineHeight:"20px" }}variant="caption" display="block" gutterBottom>
          Become a Tour guide for Us</Typography>

          </Box>
         
        </Box>
        <Box sx ={{display:"flex",flexDirection:"row",justifyContent:"space-between",backgroundColor:"#0F1E32",width:"100%",height:"50px"}}>
        <Typography sx ={{marginLeft:"130px",marginTop:"20px",color:"rgba(255, 255, 255, 0.6)",fontFamily: "Mulish",fontOpticalSizing: "regular",fontStyle: "normal"}} variant="caption" display="block" gutterBottom>
        Copyright 2021 Tour Guide. All Rights Reserved</Typography>
       
          <div className="redes">
            <Redes imgUrl="/images/face.svg"></Redes>
            <Redes  imgUrl="/images/twitter.svg"></Redes>
            <Redes  imgUrl="/images/insta.svg"></Redes>
            <Redes  imgUrl="/images/pinterest.svg"></Redes>

          </div>
          
           
          
       
          </Box>
     


      </div>
    
      






    </main>
  );
}