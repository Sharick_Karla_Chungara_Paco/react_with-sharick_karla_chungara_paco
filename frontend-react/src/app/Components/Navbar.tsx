import Image from "next/image"
import Button from "./Botton"
export default function Navbar(){
    return(
        <nav className="Nav-bar">
        <div className="logo">
            <Image src="/images/logo.svg" alt=""width={100} height={100}  />
            
        </div>
        <div className="Nav-Links">
            <ul>
                <li><a href="#">Home</a></li>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Popular Destinations</a></li>
                <li><a href="#">Our Packages</a></li>
                <li><a href="#">Help</a></li>
                <Button boton="Sign in"></Button>
                
            </ul>
        </div>
        
       </nav>

    )
}
