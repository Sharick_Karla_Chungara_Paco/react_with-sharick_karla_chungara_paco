export default function Botton(props:{boton:any,border?:boolean}){
    return(
        props.border?
        <button className="btn btn-principal btn-border">{props.boton}</button>
        :
        <button className="btn btn-principal">{props.boton}</button>
       
    )
}