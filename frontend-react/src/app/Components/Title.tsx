export default function Title(props:{title:string,parrafo:string}){
    return(
        <div className="title">
            <h3>{props.title}</h3>
            <p>{props.parrafo}</p>
        </div>
    )
}