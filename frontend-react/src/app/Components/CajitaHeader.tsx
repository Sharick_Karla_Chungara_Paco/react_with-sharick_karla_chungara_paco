
import Image from "next/image"
export default function CajitaHeader(props:{imageUrl:any,title:string,text:string}){
    return(
        <div className="Header-cajita">
            <div>
               <Image src= {props.imageUrl} alt="" width={30} height={30} />
               
               <h3>{props.title}</h3>
            </div>
            <h3>{props.text}</h3>
           
        </div>
    )      
       
}