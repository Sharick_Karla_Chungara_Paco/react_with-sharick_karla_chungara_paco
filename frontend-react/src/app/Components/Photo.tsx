import Image from "next/image";

export default function Photo(props:{imgUrl:any}){
    return(
        <Image src={props.imgUrl} alt=""width={270} height={320}  />
    )
}