
# 🍂 **PRÁCTICA N° 11** 🍂
🍃🎓**Univ.:** Sharick Karla Chungara Paco🍃

🍃👨‍🎓**Docente:** Ing. Jose David Mamani Figueroa🍃 

🍃👨‍💻**Auxiliar:** Univ. Sergio Moises Apaza Caballero 🍃

🍃🎨**Materia:** DISEÑO Y PROGRAMACIÓN GRÁFICA🍃

🍃📚**Siglas:** SIS-313 G1 🍃

🍃🗓️**Fecha:** 23/05/2024🍃

    🧡🍂🧡🍂Link del proyecto en Figma utilizando el template🧡🍂🧡🍂
                           😸CLICK EN EL MISHI😸

[![🥒🌱🥬](https://github.com/sharick-14/imgenes-prc4/assets/164090219/15c0eba9-0c52-49a6-b87b-db6608591bcc)](https://www.figma.com/design/lRWd1IbSggNCjSUEmPMPcN/Tour-Guide---travel-agency%2Ftravel-booking-website-(Community)?node-id=37-2&t=qFwwCmdOehoyfQtA-0)

 # 🌱Pregunta 2🌱

    🧡🍂🧡🍂🧡🍂🧡🍂🧡🍂🧡🍂 Header 🧡🍂🧡🍂🧡🍂🧡🍂🧡🍂🧡🍂

# Header          
![Header](https://github.com/sharick-14/imgenes-prc4/assets/164090219/ab4a544c-800f-4f4d-9dd6-cd2780beaf0b)
Dentro del Header se indentifica tres componentes, que son los siguientes:
## 🍁Navbar
Dentro de este componentes se indentifica el nav
![Navbar](https://github.com/sharick-14/imgenes-prc4/assets/164090219/0d223626-e870-46ab-8b43-a6a6512333af)
## 🍁Button
Dentro de este componente estan los botones, que se repiten demasiadas veces en mi templete, por lo tanto lo hice un componente, que el boton tiene las siguientes propiedades:
* 1  El color amarillo 
* 2  La letra
* 3 El color de la letra
**Boton del nav**
![boton1](https://github.com/sharick-14/imgenes-prc4/assets/164090219/e84742c5-7169-4bd9-8be2-4172b13d55b9)
**Boton del componente de la *CajitaHeader***
![boton2](https://github.com/sharick-14/imgenes-prc4/assets/164090219/2d2ce298-6216-480c-b882-4f9cd4946887)
**Botones del body**
![Captura de pantalla 2024-05-24 002136](https://github.com/sharick-14/imgenes-prc4/assets/164090219/019390e0-231d-4020-a0e9-70945212ee70)

![Captura de pantalla 2024-05-24 002140](https://github.com/sharick-14/imgenes-prc4/assets/164090219/c791255c-6f23-48a8-93f0-4f609a4ae32f)


![Captura de pantalla 2024-05-24 002611](https://github.com/sharick-14/imgenes-prc4/assets/164090219/ac1318eb-a8ff-4994-9dae-bc0d583b6e5e)

## 🍁CajitaHeader
Dentro del componente se repiten varias cosas, por eso lo tome como un componente:
Las pripiedades son las siguientes
* 1 Los iconos
* 2 El titulo que esta justo a su a lado del icono
* 3 Esta el texto debajo del icono y el titulo
Y asi estos tres se repiten por esa razon lo tome como un componete.

![CajitaHeader](https://github.com/sharick-14/imgenes-prc4/assets/164090219/6fc218b3-5dc9-4369-92f4-12eed2822a25)
## 🍁El titilo y el parrafo
Durante todo mi templete estas dos propiedades se repiten, tienen variaciones, pero son la misma estrcutura por lo tanto esto ya es un componente.
![titilo y parrafo](https://github.com/sharick-14/imgenes-prc4/assets/164090219/9f19cb9a-baf4-4c5a-b7cd-80c854ec446f)

# Body
![Captura de pantalla 2024-05-23 235457](https://github.com/sharick-14/imgenes-prc4/assets/164090219/188603d1-d539-4d88-b1e0-33c58e864944)

Los componentes que podemos identicar son los siguientes:
## 🍁Botones
Los botones que encontramos son varios y se repiten ademas tienen las mismas propiedades:

![Captura de pantalla 2024-05-24 000041](https://github.com/sharick-14/imgenes-prc4/assets/164090219/d040ba1f-4d05-44b7-8142-827aea70a9b4)

## 🍁BotonesLink
Es un componente ya que dentro de la cajita que estan este componente se repiten:
* 1 El iconos
* 2 * El titulo
A pesar de que no tienen el mismo color tienen una misma estructura y propiedades.Y por lo tanto lo podemos tomar como un componente.


![Captura de pantalla 2024-05-24 000452](https://github.com/sharick-14/imgenes-prc4/assets/164090219/83bd3617-b604-4989-bb00-374a8c82c046)

## 🍁Travel

![Viajes](https://github.com/sharick-14/imgenes-prc4/assets/164090219/167780cf-ec8e-43a5-aa02-2b4ce1a990c6)
Este componentes tiene varias propiedades, por lo que ya se considera un componente inmediatamente, las cuales son:
* 1 las images
* 2 El titulo
* 3 Dentro de esta estan otro componete ya qeu se repiten los mismo en todos los demas:
**Travel2**
* 3.1 Los iconos
* 3.2 El texto

![Captura de pantalla 2024-05-24 001329](https://github.com/sharick-14/imgenes-prc4/assets/164090219/2eba358f-d4d1-4765-bdf5-85448c489b4b)
* Incluso podemos identificar estos domponentes ya que en los 4 cuadros se repetienen lo mismo
![Captura de pantalla 2024-05-24 001749](https://github.com/sharick-14/imgenes-prc4/assets/164090219/c48ba22a-100e-4a7f-8fe7-91beca28a3b8)

* Estos dos botones lo podemos identificar como  otro componente
![Captura de pantalla 2024-05-24 002249](https://github.com/sharick-14/imgenes-prc4/assets/164090219/8ac76992-5e14-461d-a498-df206e68ad00)


## 🍁Images
Es otro componente ya que apesar de ser images lo podemos de tomar como un componete, por que se repite.
![Captura de pantalla 2024-05-24 002754](https://github.com/sharick-14/imgenes-prc4/assets/164090219/d628eb71-c3f5-4e0d-920a-90877c7e793d)


## 🍁Botones2
Apesar de tener un componente de botones, mejro lo tomaremso como otro componete, ya para evitar complicaciones.
Este compiente si se repute en el templete.

![Captura de pantalla 2024-05-24 002935](https://github.com/sharick-14/imgenes-prc4/assets/164090219/f1eaac43-e25f-4284-86ae-8d6c5c2801a2)

## 🍁Stories
A simple vista ya es un componete ya que tiene
* 1 Una imagen
* 2 Foto de perfil junto con su nombre
* 3 El titulo
Las propiedades Mencionadas se repiten.

![Captura de pantalla 2024-05-24 003110](https://github.com/sharick-14/imgenes-prc4/assets/164090219/fb8f8d50-a474-40c4-ae9a-e6e7d272f798)
## 🍁Footer
Esto tomamos como un componente ya que se repiten varias cosas, y para un mejor diseño, es ,mejor manejarla como un componente.

![Captura de pantalla 2024-05-24 003135](https://github.com/sharick-14/imgenes-prc4/assets/164090219/dd74637c-d879-47ca-82b1-8caf5016cb5b) 


                        TAREA COMPLETA 🌟🌟🌟🌟🌟

